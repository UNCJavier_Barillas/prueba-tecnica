package com.jram.demo.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jram.demo.Entity.CustomerCR;
import com.jram.demo.Repository.CRRepository;

@Service
public class CRService implements ServiceCommon<CustomerCR> {

	@Autowired CRRepository crRepository;
	
	@Override
	public List<CustomerCR> getAll() {
		return crRepository.findAll();
	}

	@Override
	public CustomerCR save(CustomerCR t) {
		return crRepository.save(t);
	}

	@Override
	public CustomerCR update(Long id, CustomerCR t) {
		return (findOne(id) != null) ? crRepository.save(t) : null ;
	}

	@Override
	public void delete(CustomerCR t) {
		crRepository.delete(t) ;
	}

	@Override
	public Optional<CustomerCR> findOne(Long id) {
		return crRepository.findById(id);
	}

}
