package com.jram.demo.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jram.demo.Entity.CustomerJAM;
import com.jram.demo.Repository.JAMRepository;

@Service
public class JAMService implements ServiceCommon<CustomerJAM> {

	@Autowired JAMRepository jamRepository;
	
	@Override
	public List<CustomerJAM> getAll() {
		return jamRepository.findAll();
	}

	@Override
	public CustomerJAM save(CustomerJAM t) {
		return jamRepository.save(t);
	}

	@Override
	public CustomerJAM update(Long id, CustomerJAM t) {
		return (findOne(id) != null) ? jamRepository.save(t) : null ;
	}

	@Override
	public void delete(CustomerJAM t) {
		jamRepository.delete(t) ;
	}

	@Override
	public Optional<CustomerJAM> findOne(Long id) {
		return jamRepository.findById(id);
	}

}
