package com.jram.demo.Service;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jram.demo.Entity.CustomerJAM;

@Service
public class JAMServiceRest {

	private final RestTemplate restTemplate;
	public String url = "http://localhost:8082/jam";

	public JAMServiceRest(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public List<CustomerJAM> getCustomerJAM() {
		ResponseEntity<CustomerJAM[]> response = restTemplate.getForEntity(url, CustomerJAM[].class);
		CustomerJAM[] CustomerJAM = response.getBody();
		return Arrays.asList(CustomerJAM);
	}

	public CustomerJAM createCustomerJAM(CustomerJAM customerJAM) {
		CustomerJAM response = restTemplate.postForObject(url, customerJAM, CustomerJAM.class);
		return response;
	}

	public CustomerJAM updateCustomerJAM(CustomerJAM customerJAM, Long id) {
		restTemplate.put(url + "/" + id, customerJAM, customerJAM.getId());
		return customerJAM;
	}

}
