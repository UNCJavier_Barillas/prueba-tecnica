package com.jram.demo.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jram.demo.Entity.CustomerGT;
import com.jram.demo.Repository.GTRepository;

@Service
public class GTService implements ServiceCommon<CustomerGT> {

	@Autowired GTRepository gtRepository;
	
	@Override
	public List<CustomerGT> getAll() {
		return gtRepository.findAll();
	}

	@Override
	public CustomerGT save(CustomerGT t) {
		return gtRepository.save(t);
	}

	@Override
	public CustomerGT update(Long id, CustomerGT t) {
		return (findOne(id) != null) ? gtRepository.save(t) : null ;
	}

	@Override
	public void delete(CustomerGT t) {
		gtRepository.delete(t) ;
	}

	@Override
	public Optional<CustomerGT> findOne(Long id) {
		return gtRepository.findById(id);
	}

}
