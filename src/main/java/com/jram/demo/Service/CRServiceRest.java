package com.jram.demo.Service;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jram.demo.Entity.CustomerCR;

@Service
public class CRServiceRest {

	private final RestTemplate restTemplate;
	public String url = "http://localhost:8082/cr";

	public CRServiceRest(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public List<CustomerCR> getCustomerCR() {
		ResponseEntity<CustomerCR[]> response = restTemplate.getForEntity(url, CustomerCR[].class);
		CustomerCR[] customerCR = response.getBody();
		return Arrays.asList(customerCR);
	}

	public CustomerCR createCustomerCR(CustomerCR customerCR) {
		CustomerCR response = restTemplate.postForObject(url, customerCR, CustomerCR.class);
		return response;
	}

	public CustomerCR updateCustomerCR(CustomerCR customerCR, Long id) {
		restTemplate.put(url + "/" + id, customerCR, customerCR.getId());
		return customerCR;
	}

}
