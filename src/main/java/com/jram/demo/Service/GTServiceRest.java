package com.jram.demo.Service;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jram.demo.Entity.CustomerGT;

@Service
public class GTServiceRest {

	private final RestTemplate restTemplate;
	public String url = "http://localhost:8082/gt";

	public GTServiceRest(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public List<CustomerGT> getCustomerGT() {
		ResponseEntity<CustomerGT[]> response = restTemplate.getForEntity(url, CustomerGT[].class);
		CustomerGT[] customerGT = response.getBody();
		return Arrays.asList(customerGT);
	}

	public CustomerGT createCustomerGT(CustomerGT customerGT) {
		CustomerGT response = restTemplate.postForObject(url, customerGT, CustomerGT.class);
		return response;
	}

	public CustomerGT updateCustomerGT(CustomerGT customerGT, Long id) {
		restTemplate.put(url + "/" + id, customerGT, customerGT.getId());
		return customerGT;
	}

}
