package com.jram.demo.Service;

import java.util.List;
import java.util.Optional;

public interface ServiceCommon<T> {

	public List<T> getAll();
	
	public T save(T t);
	
	public Optional<T> findOne(Long id);
	
	public T update (Long id, T t);
	
	public void delete (T t);
	
}
