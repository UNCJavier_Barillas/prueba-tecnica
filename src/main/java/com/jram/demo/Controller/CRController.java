package com.jram.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jram.demo.Entity.CustomerCR;
import com.jram.demo.Service.ServiceCommon;

@RestController
@RequestMapping("/cr")
public class CRController {

	private final Logger LOG = LoggerFactory.getLogger(CRController.class);

	@Autowired ServiceCommon<CustomerCR> service;
	
	@GetMapping
    public ResponseEntity<List<CustomerCR>> getAll() {
		LOG.info("Controller: /cr - Method: GET - Funct: GetAll");
		return ResponseEntity.ok(service.getAll());
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<CustomerCR> getById(@PathVariable Long id) {
		LOG.info("Controller: /cr/{id} - Method: GET - Funct: getById");
		Optional<CustomerCR> customerCR = service.findOne(id);
        if (customerCR.isPresent()) {
        	return ResponseEntity.ok(customerCR.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@PostMapping
    public ResponseEntity<CustomerCR> create(@RequestBody CustomerCR customerCR) {
		LOG.info("Controller: /cr - Method: POST - Funct: create");
		
		CustomerCR customerCRNew = service.save(customerCR);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerCRNew);
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<CustomerCR> update(@PathVariable Long id, @RequestBody CustomerCR customerCRUpdated) {
		LOG.info("Controller: /cr/{id} - Method: PUT - Funct: update");

		Optional<CustomerCR> customerCROptional = service.findOne(id);
        if (customerCROptional.isPresent()) {
        	CustomerCR customerCRExist = customerCROptional.get();
        	
        	customerCRExist.setFirstName(customerCRUpdated.getFirstName());
        	customerCRExist.setLastName(customerCRUpdated.getLastName());
        	customerCRExist.setBirthday(customerCRUpdated.getBirthday());
        	customerCRExist.setGender(customerCRUpdated.getGender());
        	customerCRExist.setCellphone(customerCRUpdated.getCellphone());
        	customerCRExist.setHomePhone(customerCRUpdated.getHomePhone());
        	customerCRExist.setProfession(customerCRUpdated.getProfession());
        	customerCRExist.setIncomes(customerCRUpdated.getIncomes());
        	

        	CustomerCR customerCRFinal = service.save(customerCRExist);
            return ResponseEntity.ok(customerCRFinal);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/{id}")
    public ResponseEntity<CustomerCR> delete(@PathVariable Long id) {
		LOG.info("Controller: /cr/{id} - Method: DELETE - Funct: delete");

		Optional<CustomerCR> customerCROptional = service.findOne(id);
        if (customerCROptional.isPresent()) {
            service.delete(customerCROptional.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
