package com.jram.demo.Controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jram.demo.Entity.Customer;
import com.jram.demo.Entity.CustomerCR;
import com.jram.demo.Entity.CustomerGT;
import com.jram.demo.Entity.CustomerJAM;
import com.jram.demo.Entity.Header;
import com.jram.demo.Entity.Request;
import com.jram.demo.Entity.ResponseGeneric;
import com.jram.demo.Service.CRServiceRest;
import com.jram.demo.Service.GTServiceRest;
import com.jram.demo.Service.JAMServiceRest;

@RestController
@RequestMapping("/common")
public class CommonController {

	private final Logger LOG = LoggerFactory.getLogger(CommonController.class);

	@Autowired(required = true)
	private GTServiceRest serviceGT;
	@Autowired(required = true)
	private CRServiceRest serviceCR;
	@Autowired(required = true)
	private JAMServiceRest serviceJAM;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/{country}")
	public ResponseEntity<?> getAll(@PathVariable String country) {
		LOG.info("Controller: /common - Method: GET - Funct: GetAll");
		Map<String, Object> response = new HashMap<>();

		try {

			switch (country) {
			case "GT": {
				Header countryHeader = new Header();
				countryHeader.setCountry(country);
				response.put("Response", new ResponseGeneric(countryHeader, serviceGT.getCustomerGT()));
				LOG.info("Funct: getAll _ Value: GT");
				return ResponseEntity.ok(response);
			}
			case "CR": {
				Header countryHeader = new Header();
				countryHeader.setCountry(country);
				response.put("Response", new ResponseGeneric(countryHeader, serviceCR.getCustomerCR()));
				LOG.info("Funct: getAll _ Value: CR");
				return ResponseEntity.ok(response);
			}
			case "JAM": {
				Header countryHeader = new Header();
				countryHeader.setCountry(country);
				response.put("Response", new ResponseGeneric(countryHeader, serviceJAM.getCustomerJAM()));
				LOG.info("Funct: getAll _ Value: JAM");
				return ResponseEntity.ok(response);
			}

			default:
				LOG.info("Funct: getAll _ Value: Without value to evaluate");
				return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
			}

		} catch (Exception e) {
			LOG.info("Funct: getAll _ Value: Without value to evaluate");
			return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Request<Customer> request) {
		LOG.info("Controller: /common - Method: POST - Funct: create");

		Map<String, Object> response = new HashMap<>();

		try {

			switch (request.getHeader().getCountry()) {
			case "GT": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerGT customerGT = new CustomerGT(request.getBody().getCostumerIdentification(),
						request.getBody().getFirstName(), request.getBody().getLastName(),
						request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response",
						new ResponseGeneric(countryHeader, Arrays.asList(serviceGT.createCustomerGT(customerGT))));

				LOG.info("Funct: create _ Value: GT");

				return ResponseEntity.ok(response);

			}
			case "CR": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerCR customerCR = new CustomerCR(request.getBody().getCostumerIdentification(),
						request.getBody().getFirstName(), request.getBody().getLastName(),
						request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response",
						new ResponseGeneric(countryHeader, Arrays.asList(serviceCR.createCustomerCR(customerCR))));

				LOG.info("Funct: create _ Value: CR");

				return ResponseEntity.ok(response);

			}
			case "JAM": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerJAM customerJAM = new CustomerJAM(request.getBody().getCostumerIdentification(),
						request.getBody().getFirstName(), request.getBody().getLastName(),
						request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response",
						new ResponseGeneric(countryHeader, Arrays.asList(serviceJAM.createCustomerJAM(customerJAM))));

				LOG.info("Funct: create _ Value: JAM");

				return ResponseEntity.ok(response);

			}
			default:
				LOG.info("Funct: create _ Value: Without value to evaluate");
				return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
			}

		} catch (Exception e) {
			LOG.info("Funct: create _ Value: Without value to evaluate");
			return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Request<Customer> request) {
		LOG.info("Controller: /common - Method: PUT - Funct: update");

		Map<String, Object> response = new HashMap<>();

		try {

			switch (request.getHeader().getCountry()) {
			case "GT": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerGT customerGT = new CustomerGT(request.getBody().getId(),
						request.getBody().getCostumerIdentification(), request.getBody().getFirstName(),
						request.getBody().getLastName(), request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response",
						new ResponseGeneric(countryHeader, Arrays.asList(serviceGT.updateCustomerGT(customerGT, id))));

				LOG.info("Funct: update _ Value: GT");

				return ResponseEntity.ok(response);
			}
			case "CR": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerCR customerCR = new CustomerCR(request.getBody().getId(),
						request.getBody().getCostumerIdentification(), request.getBody().getFirstName(),
						request.getBody().getLastName(), request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response",
						new ResponseGeneric(countryHeader, Arrays.asList(serviceCR.updateCustomerCR(customerCR, id))));

				LOG.info("Funct: update _ Value: CR");

				return ResponseEntity.ok(response);
			}
			case "JAM": {
				Header countryHeader = new Header();
				countryHeader.setCountry(request.getHeader().getCountry());

				CustomerJAM customerJAM = new CustomerJAM(request.getBody().getId(),
						request.getBody().getCostumerIdentification(), request.getBody().getFirstName(),
						request.getBody().getLastName(), request.getBody().getBirthday(), request.getBody().getGender(),
						request.getBody().getCellphone(), request.getBody().getHomePhone(),
						request.getBody().getAddressHome(), request.getBody().getProfession(),
						request.getBody().getIncomes());

				response.put("Response", new ResponseGeneric(countryHeader,
						Arrays.asList(serviceJAM.updateCustomerJAM(customerJAM, id))));

				LOG.info("Funct: update _ Value: JAM");

				return ResponseEntity.ok(response);

			}
			default:
				LOG.info("Funct: update _ Value: Without value to evaluate");
				return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
			}

		} catch (Exception e) {
			LOG.info("Funct: update _ Value: Without value to evaluate");
			return ResponseEntity.ok("{\r\n" + "    \"message_error\": \"Without value to evaluate\"\r\n" + "}");
		}
	}
}
