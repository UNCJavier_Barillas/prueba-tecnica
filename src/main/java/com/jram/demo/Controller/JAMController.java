package com.jram.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jram.demo.Entity.CustomerJAM;
import com.jram.demo.Service.ServiceCommon;

@RestController
@RequestMapping("/jam")
public class JAMController {

	private final Logger LOG = LoggerFactory.getLogger(JAMController.class);

	
	@Autowired ServiceCommon<CustomerJAM> service;
	
	@GetMapping
    public ResponseEntity<List<CustomerJAM>> getAll() {
		LOG.info("Controller: /jam - Method: GET - Funct: GetAll");
		
		return ResponseEntity.ok(service.getAll());
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<CustomerJAM> getById(@PathVariable Long id) {
		LOG.info("Controller: /jam/{id} - Method: GET - Funct: getById");

		Optional<CustomerJAM> CustomerJAM = service.findOne(id);
        if (CustomerJAM.isPresent()) {
            return ResponseEntity.ok(CustomerJAM.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@PostMapping
    public ResponseEntity<CustomerJAM> create(@RequestBody CustomerJAM customerJAM) {
		LOG.info("Controller: /jam - Method: POST - Funct: create");

		CustomerJAM customerJAMNew = service.save(customerJAM);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerJAMNew);
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<CustomerJAM> update(@PathVariable Long id, @RequestBody CustomerJAM customerJAMUpdated) {
		LOG.info("Controller: /jam/{id} - Method: PUT - Funct: update");

		Optional<CustomerJAM> customerJAMOptional = service.findOne(id);
        if (customerJAMOptional.isPresent()) {
        	CustomerJAM customerJAMExist = customerJAMOptional.get();
        	
        	customerJAMExist.setFirstName(customerJAMUpdated.getFirstName());
        	customerJAMExist.setLastName(customerJAMUpdated.getLastName());
        	customerJAMExist.setBirthday(customerJAMUpdated.getBirthday());
        	customerJAMExist.setGender(customerJAMUpdated.getGender());
        	customerJAMExist.setCellphone(customerJAMUpdated.getCellphone());
        	customerJAMExist.setHomePhone(customerJAMUpdated.getHomePhone());
        	customerJAMExist.setProfession(customerJAMUpdated.getProfession());
        	customerJAMExist.setIncomes(customerJAMUpdated.getIncomes());
        	

        	CustomerJAM customerJAMFinal = service.save(customerJAMExist);
            return ResponseEntity.ok(customerJAMFinal);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/{id}")
    public ResponseEntity<CustomerJAM> delete(@PathVariable Long id) {
		LOG.info("Controller: /jam/{id} - Method: DELETE - Funct: delete");

		Optional<CustomerJAM> customerJAMOptional = service.findOne(id);
        if (customerJAMOptional.isPresent()) {
            service.delete(customerJAMOptional.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
