package com.jram.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jram.demo.Entity.CustomerGT;
import com.jram.demo.Service.ServiceCommon;

@RestController
@RequestMapping("/gt")
public class GTController {
	
	private final Logger LOG = LoggerFactory.getLogger(GTController.class);

	@Autowired ServiceCommon<CustomerGT> service;

	@GetMapping
    public ResponseEntity<List<CustomerGT>> getAll() {
		LOG.info("Controller: /gt - Method: GET - Funct: GetAll");
		return ResponseEntity.ok(service.getAll());
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<CustomerGT> getById(@PathVariable Long id) {
		LOG.info("Controller: /gt/{id} - Method: GET - Funct: getById");

		Optional<CustomerGT> customerGT = service.findOne(id);
        if (customerGT.isPresent()) {
            return ResponseEntity.ok(customerGT.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@PostMapping
    public ResponseEntity<CustomerGT> create(@RequestBody CustomerGT customerGT) {
		LOG.info("Controller: /gt - Method: POST - Funct: create");

		CustomerGT customerGTNew = service.save(customerGT);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerGTNew);
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<CustomerGT> update(@PathVariable Long id, @RequestBody CustomerGT customerGTUpdated) {
		LOG.info("Controller: /gt/{id} - Method: PUT - Funct: update");
		
		Optional<CustomerGT> customerGTOptional = service.findOne(id);
        if (customerGTOptional.isPresent()) {
        	CustomerGT customerGTExist = customerGTOptional.get();
        	
        	customerGTExist.setFirstName(customerGTUpdated.getFirstName());
        	customerGTExist.setLastName(customerGTUpdated.getLastName());
        	customerGTExist.setBirthday(customerGTUpdated.getBirthday());
        	customerGTExist.setGender(customerGTUpdated.getGender());
        	customerGTExist.setCellphone(customerGTUpdated.getCellphone());
        	customerGTExist.setHomePhone(customerGTUpdated.getHomePhone());
        	customerGTExist.setProfession(customerGTUpdated.getProfession());
        	customerGTExist.setIncomes(customerGTUpdated.getIncomes());
        	

        	CustomerGT customerGTFinal = service.save(customerGTExist);
            return ResponseEntity.ok(customerGTFinal);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/{id}")
    public ResponseEntity<CustomerGT> delete(@PathVariable Long id) {
		LOG.info("Controller: /gt/{id} - Method: DELETE - Funct: delete");

		Optional<CustomerGT> customerGTOptional = service.findOne(id);
        if (customerGTOptional.isPresent()) {
            service.delete(customerGTOptional.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
