package com.jram.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jram.demo.Entity.CustomerGT;

@Repository
public interface GTRepository extends JpaRepository<CustomerGT, Long> {

}
