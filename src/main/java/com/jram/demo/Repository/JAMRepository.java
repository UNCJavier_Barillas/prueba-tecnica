package com.jram.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jram.demo.Entity.CustomerJAM;

@Repository
public interface JAMRepository extends JpaRepository<CustomerJAM, Long> {

}
