package com.jram.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jram.demo.Entity.CustomerCR;

@Repository
public interface CRRepository extends JpaRepository<CustomerCR, Long> {

}
