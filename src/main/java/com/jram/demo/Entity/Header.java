package com.jram.demo.Entity;

import java.io.Serializable;

public class Header implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String country;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
