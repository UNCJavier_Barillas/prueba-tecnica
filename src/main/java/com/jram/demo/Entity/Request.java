package com.jram.demo.Entity;

import java.io.Serializable;

public class Request<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private Header header;

	private T body;

	public Request(Header header, T body) {
		this.header = header;
		this.body = body;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

}
