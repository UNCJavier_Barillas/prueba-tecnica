package com.jram.demo.Entity;

import java.io.Serializable;
import java.util.Date;

public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;

	private String costumerIdentification;

	private String firstName;

	private String lastName;

	private Date birthday = new Date();

	private String gender;

	private Integer cellphone;

	private Integer homePhone;

	private String addressHome;

	private String profession;

	private Integer incomes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCostumerIdentification() {
		return costumerIdentification;
	}

	public void setCostumerIdentification(String costumerIdentification) {
		this.costumerIdentification = costumerIdentification;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getCellphone() {
		return cellphone;
	}

	public void setCellphone(Integer cellphone) {
		this.cellphone = cellphone;
	}

	public Integer getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(Integer homePhone) {
		this.homePhone = homePhone;
	}

	public String getAddressHome() {
		return addressHome;
	}

	public void setAddressHome(String addressHome) {
		this.addressHome = addressHome;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Integer getIncomes() {
		return incomes;
	}

	public void setIncomes(Integer incomes) {
		this.incomes = incomes;
	}

}
