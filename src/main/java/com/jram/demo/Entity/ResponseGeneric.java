package com.jram.demo.Entity;

import java.io.Serializable;
import java.util.List;

public class ResponseGeneric<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Header header;
	
	private List<T> body;

	public ResponseGeneric(Header header, List<T> body) {
		super();
		this.header = header;
		this.body = body;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public List<T> getBody() {
		return body;
	}

	public void setBody(List<T> body) {
		this.body = body;
	}
	

}
