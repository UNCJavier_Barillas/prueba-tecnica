package com.jram.demo.Entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "customers_gt")
public class CustomerGT implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String costumerIdentification;

	private String firstName;

	private String lastName;

	private Date birthday = new Date();

	private String gender;

	private Integer cellphone;

	private Integer homePhone;

	private String addressHome;

	private String profession;

	private Integer incomes;

	public CustomerGT() {
		super();
	}

	public CustomerGT(String costumerIdentification, String firstName, String lastName, Date birthday, String gender,
			Integer cellphone, Integer homePhone, String addressHome, String profession, Integer incomes) {
		super();
		this.costumerIdentification = costumerIdentification;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.gender = gender;
		this.cellphone = cellphone;
		this.homePhone = homePhone;
		this.addressHome = addressHome;
		this.profession = profession;
		this.incomes = incomes;
	}

	public CustomerGT(Long id, String costumerIdentification, String firstName, String lastName, Date birthday,
			String gender, Integer cellphone, Integer homePhone, String addressHome, String profession, Integer incomes) {
		super();
		this.id = id;
		this.costumerIdentification = costumerIdentification;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.gender = gender;
		this.cellphone = cellphone;
		this.homePhone = homePhone;
		this.addressHome = addressHome;
		this.profession = profession;
		this.incomes = incomes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCostumerIdentification() {
		return costumerIdentification;
	}

	public void setCostumerIdentification(String costumerIdentification) {
		this.costumerIdentification = costumerIdentification;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getCellphone() {
		return cellphone;
	}

	public void setCellphone(Integer cellphone) {
		this.cellphone = cellphone;
	}

	public Integer getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(Integer homePhone) {
		this.homePhone = homePhone;
	}

	public String getAddressHome() {
		return addressHome;
	}

	public void setAddressHome(String addressHome) {
		this.addressHome = addressHome;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Integer getIncomes() {
		return incomes;
	}

	public void setIncomes(Integer incomes) {
		this.incomes = incomes;
	}

}
